const doanhNghiep = "Doanh Nghiệp";
const nhaDan = "Nhà Dân";

function hienThiKetNoi() {
  var khachHang = document.getElementById("khachHang").value;
  var inputSoKetNoi = document.getElementById("soKetNoi");

  khachHang == doanhNghiep
    ? inputSoKetNoi.classList.remove("d-none")
    : inputSoKetNoi.classList.add("d-none");
}

function tinhPhiHoaDon(khachHang) {
  if (khachHang == nhaDan) {
    return 4.5;
  } else {
    return 15;
  }
}

function tinhPhiCoBan(khachHang) {
  switch (khachHang) {
    case nhaDan: {
      return 20.5;
    }
    case doanhNghiep: {
      return 75;
    }
  }
}

function tinhKenhCaoCap(khachHang) {
  if (khachHang == nhaDan) {
    return 7.5;
  } else {
    return 50;
  }
}

function tinhTien() {
  var khachHang = document.getElementById("khachHang").value;
  var maKhachHang = document.getElementById("maKhachHang").value;
  var soKetNoi = document.getElementById("ketNoi").value * 1;
  var soKenhCaoCap = document.getElementById("kenhCaoCap").value * 1;
  // console.log({ khachHang, maKhachHang, soKetNoi, soKenhCaoCap });

  var phiHoaDon = tinhPhiHoaDon(khachHang);
  var phiCoBan = tinhPhiCoBan(khachHang);
  var phiKenhCaoCap = tinhKenhCaoCap(khachHang);
  // console.log({ phiHoaDon, phiCoBan, phiKenhCaoCap });
  var phiDoanhNghiepSau10KetNoi = 5;

  var result = 0;

  if (khachHang == nhaDan) {
    result = phiHoaDon + phiCoBan + phiKenhCaoCap * soKenhCaoCap;
  } else if (khachHang == doanhNghiep && soKetNoi <= 10) {
    result = phiHoaDon + phiCoBan + phiKenhCaoCap * soKenhCaoCap;
  } else {
    result =
      phiHoaDon +
      phiCoBan +
      phiKenhCaoCap * soKenhCaoCap +
      (soKetNoi - 10) * phiDoanhNghiepSau10KetNoi;
  }

  //   console.log("result: ", result);

  document.getElementById(
    "result"
  ).innerHTML = `Mã khách hàng : <span class="text-success">${maKhachHang}</span> ; Tiền cáp : <span class="text-danger">${result} $</span>`;
}
